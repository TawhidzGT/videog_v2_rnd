package com.abir.videog.http;

import com.abir.videog.model.VideoViewStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("player/status")
    Call<VideoViewStatus> viewLog(@Header("Authorization") String apiKey,@Body VideoViewStatus videoStatus);
}
