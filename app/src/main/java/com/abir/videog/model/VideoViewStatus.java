package com.abir.videog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoViewStatus {

    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("content_id")
    @Expose
    private String contentId;
    @SerializedName("track_time")
    @Expose
    private String trackTime;
    @SerializedName("player_status")
    @Expose
    private String playerStatus;
    @SerializedName("from_status")
    @Expose
    private String from_status;
    @SerializedName("from_time")
    @Expose
    private String from_time;
    @SerializedName("to_status")
    @Expose
    private String to_status;
    @SerializedName("to_time")
    @Expose
    private String to_time;



    public VideoViewStatus(String mobileNo, String contentId, String trackTime, String playerStatus) {
        this.mobileNo = mobileNo;
        this.contentId = contentId;
        this.trackTime = trackTime;
        this.playerStatus = playerStatus;
    }

    public VideoViewStatus(String mobileNo, String contentId, String from_status,String from_time,String to_status,String to_time) {
        this.mobileNo = mobileNo;
        this.contentId = contentId;
        this.from_status=from_status;
        this.from_time=from_time;
        this.to_status=to_status;
        this.to_time=to_time;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getTrackTime() {
        return trackTime;
    }

    public void setTrackTime(String trackTime) {
        this.trackTime = trackTime;
    }

    public String getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(String playerStatus) {
        this.playerStatus = playerStatus;
    }

    public String getFrom_status() {
        return from_status;
    }

    public void setFrom_status(String from_status) {
        this.from_status = from_status;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getTo_status() {
        return to_status;
    }

    public void setTo_status(String to_status) {
        this.to_status = to_status;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }
}